/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betsafe.services;

import com.betsafe.db.DBTransactions;
import com.betsafe.utils.Logging;
import com.betsafe.utils.Props;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author antonio
 */
public class OddsConsumer {

    private final Logging logger;
    private final Props props;

    public OddsConsumer(Logging logger, Props props) {

        this.logger = logger;
        logger.info("Consumer:: Creating Odds Consumer");
        this.props = props;
    }

    public void fetchOdds(String apiAccount, String apiPassword,
            String eventsProgramCode, int bookmarkerId) {

        OkHttpClient client = new OkHttpClient.Builder()
                                    .connectTimeout(20, TimeUnit.SECONDS)
                                    .writeTimeout(20, TimeUnit.SECONDS)
                                    .readTimeout(20, TimeUnit.SECONDS)
                                    .build();

        String lastModified = "1900-01-01T00:00:00";

        ArrayList<HashMap<String, String>> lastModifiedList
                = DBTransactions.query("SELECT * FROM odds_last_modified ORDER "
                        + "BY 1 DESC LIMIT 1");

        if (!lastModifiedList.isEmpty()) {
            lastModified = lastModifiedList.get(0).get("last_modified");
            logger.info("Last odds modification date is " + lastModified);
        }

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType,
                "{\"APIAccount\":\"" + apiAccount + "\","
                + "\"APIPassword\":\"" + apiPassword + "\","
                + "\"EventsProgramCode\":\"" + eventsProgramCode + "\","
                + "\"LanguageID\":\"2\","
                + "\"DateLastModify\":\"" + lastModified + "\","
                + "\"IDBookmaker\":\"" + bookmarkerId + "\"}");

        Request request = new Request.Builder()
                .url("https://" + Props.getAPIHostURL() + "/api/eventprogram/GetEventsProgramV6")
                .post(body)
                .addHeader("content-type", "application/json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            String responseJSON = response.body().string();
            JSONObject json = new JSONObject(responseJSON);

            int resultCode = json.getInt("ResultCode");

            if (resultCode == 1) {

                String lastServerDate = json.getString("LastServerDate");
                JSONArray sports = json.getJSONArray("Sports");
                int noOfSports = sports.length();
                int sportId, order, categoryId, categoryStatus, categoryOrder,
                        competitionId, competitionStatus, competitionOrder,
                        matchesBatchControl, numMatches, parentMatchId,
                        matchStatus, matchOrder, oddsBatchControl, numOdds,
                        oddsId, subTypeId, oddsStatus, oddTypeId;
                String sportName, sportSQL, localSportId, categoryName,
                        categorySQL, localCategoryId, competitionName,
                        competitionSQL, localCompetitionId, teams, startDate,
                        name, shortName, oddKey;
                JSONArray categories, competitions, matches, odds;
                JSONObject categoryObject, competitionObject, matchObject,
                        oddObject;
                double oddValue, sbv;

                ArrayList<String> matchesBatch, oddsBatch;

                for (int i = 0; i < noOfSports; i++) {

                    JSONObject sportObject = sports.getJSONObject(i);

                    sportId = sportObject.getInt("SportID");
                    sportName = sportObject.getString("Sport");
                    sportName = sportName.replace("'", "\\'");
                    order = sportObject.getInt("Order");
                    categories = sportObject.getJSONArray("Groups");

                    sportSQL = "INSERT INTO sport (`sport_name`,`created_by`, "
                            + "`priority`, `betradar_sport_id`) VALUES ('" + sportName + "',"
                            + " 'ISOLUTIONS-ANTO', '" + order + "', '" + sportId + "') "
                            + "ON DUPLICATE KEY UPDATE `priority` = '" + order + "'";

                    localSportId = DBTransactions.update(sportSQL);

                    if (localSportId == null) {
                        localSportId = String.valueOf(
                                DBTransactions.getForeignKey("Sport",
                                        "betradar_sport_id",
                                        String.valueOf(sportId)));
                    }

                    for (int j = 0; j < categories.length(); j++) {

                        categoryObject = categories.getJSONObject(j);

                        categoryId = categoryObject.getInt("GroupID");
                        categoryStatus = categoryObject.getInt("Status");
                        categoryName = categoryObject.getString("Group");
                        categoryName = categoryName.replace("'", "\\'");
                        categoryOrder = categoryObject.getInt("Order");
                        competitions = categoryObject.getJSONArray("Events");

                        categorySQL = "INSERT INTO category (`category_name`,"
                                + "`status`, `sport_id`, `created_by`, "
                                + "`betradar_category_id`, `priority`) VALUES "
                                + "('" + categoryName + "', '" + categoryStatus + "',"
                                + "'" + localSportId + "', 'ISOLUTIONS-ANTO',"
                                + "'" + categoryId + "', '" + categoryOrder + "') "
                                + "ON DUPLICATE KEY UPDATE "
                                + "`priority` = '" + categoryOrder + "'";

                        localCategoryId = DBTransactions.update(categorySQL);

                        if (localCategoryId == null) {
                            localCategoryId = String.valueOf(
                                    DBTransactions.getForeignKey("Category",
                                            "betradar_category_id",
                                            String.valueOf(categoryId)));
                        }

                        for (int k = 0; k < competitions.length(); k++) {

                            competitionObject = competitions.getJSONObject(k);

                            competitionId = competitionObject.getInt("EventID");
                            competitionStatus = competitionObject.getInt("Status");
                            competitionName = competitionObject.getString("Event");
                            competitionName = competitionName.replace("'", "\\'");
                            competitionOrder = competitionObject.getInt("Order");
                            matches = competitionObject.getJSONArray("SubEvents");

                            competitionSQL = "INSERT INTO competition "
                                    + "(`competition_name`,`category`, `status`,"
                                    + "`category_id`, `sport_id`, `created_by`, "
                                    + "`betradar_competition_id`, `priority`, "
                                    + "`ussd_priority`) VALUES "
                                    + "('" + competitionName + "', "
                                    + "'" + categoryName + "',"
                                    + "'" + competitionStatus + "',"
                                    + "'" + localCategoryId + "',"
                                    + "'" + localSportId + "', 'ISOLUTIONS-ANTO',"
                                    + "'" + competitionId + "',"
                                    + "'" + competitionOrder + "',"
                                    + "'" + competitionOrder + "') "
                                    + "ON DUPLICATE KEY UPDATE "
                                    + "`priority` = '" + competitionOrder + "'";

                            localCompetitionId = DBTransactions.update(competitionSQL);
                            
                            if (localCompetitionId == null) {
                                localCompetitionId = String.valueOf(
                                        DBTransactions.getForeignKey("competition",
                                                "betradar_competition_id",
                                                String.valueOf(competitionId)));
                            }
                            matchesBatch = new ArrayList<>();
                            matchesBatchControl = 0;
                            numMatches = matches.length();

                            for (int l = 0; l < numMatches; l++) {

                                matchObject = matches.getJSONObject(l);

                                parentMatchId = matchObject.getInt("SubEventID");
                                matchStatus = matchObject.getInt("Status");
                                teams = matchObject.getString("SubEvent");
                                startDate = matchObject.getString("StartDate");
                                matchOrder = matchObject.getInt("Order");
                                odds = matchObject.getJSONArray("Odds");

                                if (teams.contains("-")) {
                                    matchesBatch.add(this.insertMatch(
                                            parentMatchId, teams, startDate,
                                            matchStatus, localCompetitionId,
                                            matchOrder));

                                    oddsBatch = new ArrayList<>();
                                    oddsBatchControl = 0;
                                    numOdds = odds.length();

                                    for (int m = 0; m < numOdds; m++) {

                                        oddObject = odds.getJSONObject(m);

                                        oddsId = oddObject.getInt("OddsID");
                                        name = oddObject.getString("OddsClass");
                                        name = name.replace("'", "\\'");
                                        shortName = oddObject.getString("OddsClassCode");
                                        shortName = shortName.replace("'", "\\'");
                                        sbv = oddObject.getDouble("HND");
                                        oddKey = oddObject.getString("OddsType");
                                        oddKey = oddKey.replace("'", "\\'");
                                        oddValue = oddObject.getDouble("Odds");

                                        DBTransactions.update("INSERT IGNORE INTO"
                                                + " sub_type (`subtype_name`) "
                                                + "VALUES('" + name + "')");

                                        subTypeId = this.getSubTypeFromOddClass(name);
                                        oddsStatus = oddObject.getInt("Status");
                                        oddTypeId = oddObject.getInt("OddsTypeID");

                                        if (oddsStatus == 1) {

                                            oddsBatch.add(this.insertOddType(name,
                                                    shortName, parentMatchId, sbv,
                                                    subTypeId));

                                            oddsBatch.add(this.insertOdds(oddKey,
                                                    oddValue, sbv, oddsStatus,
                                                    parentMatchId, oddTypeId, oddsId,
                                                    subTypeId));

                                            oddsBatchControl++;

                                            if (oddsBatchControl == 300
                                                    || l == (numOdds - 1)) {

                                                if (!DBTransactions.updateMultiple(oddsBatch)) {
                                                    logger.error("ERROR IN ODDS BATCH"
                                                            + " WITH THESE QUERIES\n");
                                                    oddsBatch.stream().forEach(
                                                            (query) -> logger.error(query));
                                                } else {
                                                    logger.info("Executed odds batch with "
                                                            + oddsBatchControl + " "
                                                            + "records successfully");
                                                }
                                                oddsBatchControl = 0;
                                            }
                                        }

                                    }

                                    matchesBatchControl++;

                                    if (matchesBatchControl == 300
                                            || l == (numMatches - 1)) {

                                        if (!DBTransactions.updateMultiple(matchesBatch)) {
                                            logger.error("ERROR IN MATCHES BATCH"
                                                    + " WITH THESE QUERIES\n");
                                            matchesBatch.stream().forEach(
                                                    (query) -> logger.error(query));
                                        } else {
                                            logger.info("Executed batch with "
                                                    + matchesBatchControl + " "
                                                    + "records successfully");
                                        }
                                        matchesBatchControl = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                this.updateOddsLastModified(lastServerDate);
            } else {
                logger.error("Failed to fetch odds Result Code " + resultCode
                        + " Result DESC " + json.getString("ResultDescription"));
            }

            logger.info("Successfully wrote odds records to the database");
            System.exit(0);
        } catch (IOException | JSONException ee) {
            logger.error("Exception within the fetch odds method " + ee);
        }
    }

    public String insertMatch(int parentMatchID, String match,
            String startDate, int status, String competitionID, int order) {

        String home = match.substring(0, match.indexOf("-") - 1);
        String away = match.substring(match.indexOf("-") + 1);

        home = home.replace("'", "\\'").trim();
        away = away.replace("'", "\\'").trim();

        String query = "INSERT INTO `match` (`parent_match_id`,`home_team`,`away_team`, "
                + "`start_time`,`game_id`,`competition_id`,`status`,`instance_id`,"
                + "`bet_closure`,`created_by`, `completed`,`priority`) "
                + " VALUES('" + parentMatchID + "', '" + home + "', '" + away + "',"
                + "'" + startDate.replace("T", " ") + "', '100',  '" + competitionID + "',"
                + " '" + status + "', '1', '" + startDate.replace("T", " ") + "',"
                + " 'ISOLUTIONS-ANTO', '0', '" + order + "') ON DUPLICATE KEY "
                + "UPDATE `start_time` = '" + startDate.replace("T", " ") + "', "
                + "`bet_closure` = '" + startDate.replace("T", " ") + "',"
                + "`priority` = '" + order + "'";

        return query;

    }

    public String insertOddType(String name, String shortName,
            int parentMatchId, double sbv, int subTypeId) {

        name = name.replace("'", "\\'");
        shortName = shortName.replace("'", "\\'");
        String query = "INSERT IGNORE INTO odd_type (`name`,`created_by`, "
                + "`active`,`sub_type_id`,`parent_match_id`,"
                + "`live_bet`,`short_name` ,`priority`,"
                + "`special_bet_value`) VALUES('" + name + "', "
                + "'IsolutionsAPI', '1',  '" + subTypeId + "',"
                + " '" + parentMatchId + "', '0',"
                + " '" + shortName + "', '0', '" + sbv + "')";
        return query;

    }

    public int getSubTypeFromOddClass(String OddsClass) {

        return DBTransactions.getForeignKey("sub_type", "subtype_name", OddsClass);
    }

    public String insertOdds(String oddKey, double odds, double sbv,
            int status, int parentMatchId, int oddsTypeID,
            int betradarOddsID, int subTypeId) {

        oddKey = oddKey.replace("'", "\\'");
        return "INSERT INTO event_odd (`parent_match_id`,`sub_type_id`,"
                + "`max_bet`,`odd_key`, `odd_value`,`special_bet_value`,"
                + "`betradar_odd_id`) VALUES ('" + parentMatchId + "', "
                + "'" + subTypeId + "','0.00', '" + oddKey + "', '" + odds + "',"
                + "'" + sbv + "','" + betradarOddsID + "') ON DUPLICATE KEY UPDATE "
                + "`odd_value` = '" + odds + "', betradar_odd_id = "
                + "'" + betradarOddsID + "'";
    }

    public void updateOddsLastModified(String lastModified) {

        String query = "INSERT INTO odds_last_modified VALUES(NULL, "
                + "'" + lastModified + "')";
        DBTransactions.update(query);
    }
}
